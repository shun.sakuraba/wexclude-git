#!/usr/bin/env python
# Copyright (C) 2013 Shun Sakuraba
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version. See LICENSE file for details.



import os.path
import sys
import math
import random
import copy

from optparse import OptionParser

if sys.version_info < (2, 6):
    from sets import Set
    set = Set

parser = OptionParser()

parser.add_option("-g", "--gro", dest = "gro",
                  help = "Structure file")

parser.add_option("-o", "--output", dest = "output",
                  help = "Structure file")

parser.add_option("-d", "--distance", dest = "dist", type="float", default=4.0,
                  help = "If there are no water atoms within this distance (by angstrom), the water molecules will be moved")

parser.add_option("-i", "--insertion", dest = "insdist", type="float", default=1.15,
                  help = "The minimal distance criterion used for inserting water molecules (by angstrom)")
parser.add_option("-x", "--no-insertion", dest = "ins", action="store_false", default=True, 
                  help = "Disable insertion and only remove water atoms")

parser.add_option("--num-nearby-water", dest = "maxwater", 
                  default=0, action="store", type="int",
                  help = "Water molecule is judged to be valid if there are less than or equal to this number of water molecules")

parser.add_option("-v", "--verbose", dest = "verbose", action="store_true",
                  help = "Be more verbose",
                  default = False)

(options, args) = parser.parse_args()

def log(x):
    if options.verbose:
        print >> sys.stderr, x

def warn_or_error(x, col):
    prefix = ""
    suffix = ""
    if os.isatty(sys.stderr.fileno()):
        prefix = "\x1b[" + str(col) + ";1m"
        suffix = "\x1b[0m"
    print >> sys.stderr, prefix + x + suffix

def warn(x):
    warn_or_error("Warning: " + x, 33)

def error(x):
    warn_or_error("Error: " + x, 31)
    sys.exit(1)

def safe_close(fh):
    # asynchronous NFS may miss file creation
    # esp. combined with delayed allocation (ext4, XFS)
    fh.flush()
    os.fsync(fh.fileno())
    fh.close()

# I do want that Numpy or Scipy is installed in a default Python package ...
def vsum(v):
    return v[0] + v[1] + v[2]

def vscale(sc, v):
    return [e * sc for e in v]

def vadd(v, w):
    return [v[0] + w[0], v[1] + w[1], v[2] + w[2]]

def vsub(v, w):
    return [v[0] - w[0], v[1] - w[1], v[2] - w[2]]

def vdot(v, w):
    return v[0] * w[0] + v[1] * w[1] + v[2] * w[2]

def vmuleach(v, w):
    return [v[0] * w[0], v[1] * w[1], v[2] * w[2]]

def vsquarednorm(v):
    return vdot(v, v)

def vnorm(v):
    return math.sqrt(vsquarednorm(v))

def mvmul(m, v):
    return [vsum([v[j] * m[j][i] for j in range(3)]) for i in range(3)]

def transpose(m):
    return [[m[j][i] for j in range(3)] for i in range(3)]

def invmat_upper(m):
    invx = 1.0 / m[0][0]
    invy = 1.0 / m[1][1]
    invz = 1.0 / m[2][2]
    return [[invx, 0., 0.],
            [-m[1][0] * invx * invy, invy, 0.],
            [(m[1][0] * m[2][1] * invy - m[2][0]) * invx * invz, 
             -m[2][1] * invy * invz, invz]]

if options.gro == None or options.output == None:
    error("Coordinate file or output file is not specified")

# to nm
options.dist *= 0.1
options.insdist *= 0.1

fh = open(options.gro, "rt")
header = fh.next()
natoms = int(fh.next().strip())
atoms = []
nm_to_ang = 10.0
for i in range(natoms):
    line = fh.next()
    resnum = int(line[0:5].strip())
    resname = line[5:10].strip()
    atomname = line[10:15].strip()
    atomnum = int(line[15:20].strip())
    x = float(line[20:28].strip())
    y = float(line[28:36].strip())
    z = float(line[36:44].strip())
    atoms.append((resnum, resname, atomname, atomnum, x, y, z))
periodicity = fh.next()
fh.close()

def getn(ix):
    return float(periodicity[(ix * 10):((ix + 1) * 10)].strip())

xbox = [getn(0), 0., 0.]
ybox = [0., getn(1), 0.]
zbox = [0., 0., getn(2)]

if periodicity[30:].strip() != "":
    xbox[1] = getn(3)
    xbox[2] = getn(4)
    ybox[0] = getn(5)
    ybox[2] = getn(6)
    zbox[0] = getn(7)
    zbox[1] = getn(8)

mbox = [xbox, ybox, zbox]
period = (
    mbox
    != [[0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0]])
if period:
    invbox = invmat_upper(mbox)

maxneighbour = max(options.dist, options.insdist)

# Construct cell linked list
# find appropriate nblk numbebrs

minblkdist = 1e999
for xi in (-1, 0, 1):
    for yi in (-1, 0, 1):
        for zi in (-1, 0, 1):
            ivec = [xi, yi, zi]
            if ivec == [0, 0, 0]:
                continue
            v = mvmul(mbox, ivec)
            minblkdist = min(minblkdist, vnorm(v))

# find largest integer nblk s.t.
#  minblkdist / nblk > maxneighbour
# <=> nblk < minblkdist / maxneighbour
nblk = int(math.floor(minblkdist / maxneighbour))

blockvec = [vscale(1. / nblk, mbox[i]) for i in range(3)]
invblockvec = invmat_upper(blockvec)

def block_of_vec(v):
    bp = mvmul(invblockvec, v)
    # in python, positive xblk results in positive reminder
    ibp = [int(math.floor(x)) % nblk for x in bp]
    return ibp

belong_box = {}
for xi in range(nblk):
    for yi in range(nblk):
        for zi in range(nblk):
            belong_box[(xi, yi, zi)] = []

for i in range(len(atoms)):
    (_, _, _, _, x, y, z) = atoms[i]
    bi = block_of_vec([x, y, z])
    belong_box[tuple(bi)].append(i)

# place sentinels
# too lazy...
for xi in range(nblk + 2):
    for yi in range(nblk + 2):
        for zi in range(nblk + 2):
            copyto = (xi, yi, zi)
            copyfrom = (xi % nblk, yi % nblk, zi % nblk)
            if copyfrom == copyto:
                continue
            belong_box[copyto] = belong_box[copyfrom]


def calc_dist_pos(v, j):
    (_, _, _, _, ox, oy, oz) = atoms[j]
    dv = vsub([ox, oy, oz], v)
    if period:
        prd = mvmul(invbox, dv)
        prd_fixed = [a - round(a) for a in prd]
        dv = mvmul(mbox, prd_fixed)
    d2 = vsquarednorm(dv)
    return d2

def calc_dist(i, j):
    (_, _, _, _, x, y, z) = atoms[i]
    return calc_dist_pos([x, y, z], j)

def find_neighbour(pos, dist):
    assert(dist <= maxneighbour)
    [xbase, ybase, zbase] = block_of_vec(pos)
    if xbase == 0: # wrap around
        xbase = nblk
    if ybase == 0: 
        ybase = nblk
    if zbase == 0: 
        zbase = nblk
    ret = []
    dist2 = dist * dist
    for xi in range(xbase - 1, xbase + 2):
        for yi in range(ybase - 1, ybase + 2):
            for zi in range(zbase - 1, zbase + 2):
                group = belong_box[(xi, yi, zi)]
                for a in group:
                    d2 = calc_dist_pos(pos, a)
                    if d2 < dist2:
                        ret.append(a)
    return ret

removal = []
for i in range(len(atoms)):
    (resnum, resname, atomname, atomnum, x, y, z) = atoms[i]
    if resname == "SOL" and atomname == "OW":
        for j in range(i + 1, i + 5):
            if j >= len(atoms):
                break
            (oresnum, oresname, oatomname, oatomnum, ox, oy, oz) = atoms[j]
            if oresnum != resnum or oresname != resname:
                break
        watend = j
        nwatatom = j - i
        nb = find_neighbour([x, y, z], options.dist)
        if len(nb) == watend - i:
            # water in vacuum
            continue
        num_wateratoms_nearby = 0
        for a in nb:
            if i <= a and a < watend:
                continue
            (oresnum, oresname, oatomname, oatomnum, ox, oy, oz) = atoms[a]
            if oresname == "SOL":
                num_wateratoms_nearby += 1
        num_watermolecules = num_wateratoms_nearby / nwatatom
        if num_watermolecules <= options.maxwater:
            # found buried water
            print "Found buried water (resid %d)" % resnum
            print "nb: ", nb
            print "x, y, z:", (x, y, z)
            print "xb, yb, zb:", (xbox, ybox, zbox)
            # Try to insert at random position
            if options.ins:
                while True:
                    ibx = random.uniform(0.0, 1.0)
                    iby = random.uniform(0.0, 1.0)
                    ibz = random.uniform(0.0, 1.0)
                    ivec = mvmul(mbox, [ibx, iby, ibz])
                    nb = find_neighbour(ivec, options.dist)
                    if nb == []:
                        continue
                    have_nonwater = False
                    for a in nb:
                        (oresnum, oresname, oatomname, oatomnum, ox, oy, oz) = atoms[a]
                        if oresname != "SOL":
                            have_nonwater = True
                            break
                    if have_nonwater:
                        continue
                    # found non-water place. Check whether there is a room for insertion
                    nb = find_neighbour(ivec, options.insdist)
                    if nb != []:
                        # too close to existing atoms
                        continue
                    dx = (ivec[0] - x)
                    dy = (ivec[1] - y)
                    dz = (ivec[2] - z)
                    for j in range(i, watend):
                        (oresnum, oresname, oatomname, oatomnum, ox, oy, oz) = atoms[j]
                        atoms[j] = (oresnum, oresname, oatomname, oatomnum, 
                                    ox + dx, oy + dy, oz + dz)
                    break
            else:
                removal += range(i, watend)
                    
fh = open(options.output, "wt")
fh.write(header)
fh.write("%d\n" % (natoms - len(removal)))

atomnum = 1
for i in range(natoms):
    if i in removal:
        continue
    (resnum, resname, atomname, _, x, y, z) = atoms[i]
    if atomnum >= 100000:
        atomnum = 0
    field = "%5d%-5s%5s%5d%8.3f%8.3f%8.3f\n" % (resnum, resname, atomname, atomnum, x, y, z)
    fh.write(field)
    atomnum += 1

if (mbox[0][1] == 0.0 and mbox[0][2] == 0.0 and 
    mbox[1][0] == 0.0 and mbox[1][2] == 0.0 and
    mbox[2][0] == 0.0 and mbox[2][1] == 0.0) :
    fh.write("%10.5f%10.5f%10.5f\n" % (mbox[0][0], mbox[1][1], mbox[2][2]))
else:
    fh.write("%10.5f%10.5f%10.5f%10.5f%10.5f%10.5f%10.5f%10.5f%10.5f\n" 
             % (mbox[0][0], mbox[1][1], mbox[2][2],
                mbox[0][1], mbox[0][2],
                mbox[1][0], mbox[1][2],
                mbox[2][0], mbox[2][1]))

safe_close(fh)
                    
                    
                    

                
    
    

